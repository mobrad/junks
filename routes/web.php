<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\User;
use App\Admin;
use Illuminate\Support\Facades\Redis;
use App\Events\UserEvent;
Route::get('/search','SearchController@search');
Route::post('users/logout','Auth\LoginController@userLogout')->name('users.logout');
Route::group(['namespace' => 'Auth'],function (){
    Route::get('/redirect', 'SocialAuthFacebookController@redirect');
    Route::get('/callback', 'SocialAuthFacebookController@callback');
});
Route::group(['prefix' => 'admin','namespace'=>'Admin'], function() {
    Route::get('/','HomeController@index')->name('admin.dashboard');
    Route::get('/login','LoginController@showLoginForm');
    Route::get('/junks','JunkController@index')->name('junk');

    Route::post('/junks/status/{id}','JunkController@status')->name('status');
    // Route::get('/category','JunkController@category')->name('')
    Route::get('/users','UserController@index')->name('users');
    Route::post('/login','LoginController@login')->name('admin.login');
    Route::post('/logout','LoginController@logout')->name('admin.logout');
    Route::resource('/fixcat', 'FixCatController',['only' =>['create','update','destroy','index','store']]);
    Route::resource('/clients', 'ClientController',['only' =>['create','update','destroy','index','store']]);
    Route::resource('/partners', 'PartnerController',['only' => ['create','update','destroy','index','store']]);
    Route::resource('/testimonials', 'TestimonialController',['only' => ['create','update','destroy','index','store']]);
    Route::delete('/{user}/notifications',function (Admin $admin){
        return $admin->notifications->delete();
    });
});
Auth::routes();

// @todo - make a more decent route with category and name
Route::get('/', 'HomeController@index')->name('home');
Route::get('/junk/{category}/{junk}','HomeController@show')->name('junk');
Route::post('/junk/{id}/view', 'HomeController@view')->name('view');
Route::group(['prefix' => 'junk','namespace' => 'User'], function(){
    Route::get('/sell','JunkController@sell')->name('sell.junk');
    Route::post('/sell','JunkController@sellJunk')->name('sell');
    Route::get('/{fixcat}','JunkController@junkCategory')->where(['fixcat' => '[A-Za-z_-]+'])->name('junk.category');
});




// All routings for the users
Route::group(['prefix' => 'user','namespace' => 'User'],function(){
	Route::get('/','DashboardController@index')->name('users.dashboard');

    Route::resource('/user-profile','ProfileController',['only' => ['show','update']]);
    Route::resource('/junks','JunkController',['only' => ['create','update','destroy','index','store']]);
    Route::get('/junks/category','JunkController@category');
    Route::post('/user-profile/image/{id}','ProfileController@image');


});
