@extends('site.layouts.app')

@section('content')
	<style>
		.lg-heading {
			font-family: 'Roboto Light';
			font-size: 5em;
			display: inline-block;
			color: white;
			padding-right: .5em;
			padding-left: .5em;
			background: black;
			border-radius: 3px;
			box-shadow: 0 3px 4px rgba(0,0,0,0.3);
		}
 
		.md-heading {

			font-size: 2em;
			font-family: 'Roboto Condensed';

		}

		#msg-content {
			margin-top: 40px;
			margin-bottom: 60px;
		}

	</style>
	<div class="container">
		
		<div class="row">
			

			<div class="col-sm-12 col-md-10 col-md-offset-2">
				<div class="row">

					<div id="msg-content" class="clearfix">

						<div class="col-sm-5">
							<img class="img-responsive" src="{{ asset('img/404-snow.gif') }}"/>
						</div>

						<div class="col-sm-7">
							<h3 class="md-heading">
								Resource not found <br/>
								Please be sure to head to the <br/>
								<a href="/">Home Page</a>
							</h3>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
@endsection