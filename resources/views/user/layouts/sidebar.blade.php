<div class="left-sidebar fixed-sidebar bg-black-300 box-shadow">
                        <div class="sidebar-content">
                            
                            <!-- /.user-info -->

                            <div class="sidebar-nav">
                                <ul class="side-nav color-gray">
                                    
                                    <li class="nav-header">
                                        <span class="">Main Category</span>
                                    </li>

                                    <router-link tag="li" to="/"><a href="color-primary.html"><i class="fa fa-user"></i> <span>Dashboard</span></a></router-link>
                                    <router-link tag="li" to="/junks"><a><i class="fa fa-trash-o"></i> <span>My Junks</span></a></router-link>
                                    <router-link tag="li" to="/Instructions"><a><i class="fa fa-bank"></i> <span>Instructions</span></a></router-link>
                                    
                                </ul>
                                <!-- /.side-nav -->
                                <div class="purchase-btn hidden-sm hidden-xs">
                                    <a href="/" target="_blank" class="btn btn-success btn-labeled">View Home Page<span class="btn-label btn-label-right"><i class="fa fa-check"></i></span></a>
                                </div>
                            </div>
                            <!-- /.sidebar-nav -->
                        </div>
                        <!-- /.sidebar-content -->
                    </div>