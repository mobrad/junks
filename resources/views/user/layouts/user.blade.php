<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title> {{Auth::user()->name}}</title>

        <!-- ========== COMMON STYLES ========== -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{asset('css/admin/font-awesome.min.css')}}" media="screen" >
        <link rel="stylesheet" href="{{asset('css/admin/animate-css/animate.min.css')}}" media="screen" >
        <link rel="stylesheet" href="{{asset('css/admin/lobipanel/lobipanel.min.css')}}" media="screen" >

        <!-- ========== PAGE STYLES ========== -->
        <link rel="stylesheet" href="{{asset('css/admin/prism/prism.css')}}" media="screen" > <!-- USED FOR DEMO HELP - YOU CAN REMOVE IT -->
        <link rel="stylesheet" href="{{asset('css/admin/icheck/skins/line/blue.css')}}" >
        <link rel="stylesheet" href="{{asset('css/admin/icheck/skins/line/red.css')}}" >
        <link rel="stylesheet" href="{{asset('css/admin/icheck/skins/line/green.css')}}" >
        <link rel="stylesheet" href="{{asset('css/admin/icheck/skins/square/blue.css')}}" >
        <link rel="stylesheet" href="{{asset('css/admin/icheck/skins/square/red.css')}}" >
        <link rel="stylesheet" href="{{asset('css/admin/icheck/skins/square/green.css')}}" >
         <link rel="stylesheet" href="{{asset('css/admin/icheck/skins/flat/flat.css')}}" >
        <link rel="stylesheet" href="{{asset('css/admin/icheck/skins/line/blue.css')}}" >

        <!-- ========== THEME CSS ========== -->
        <link rel="stylesheet" href="{{asset('css/admin/main.css')}}" media="screen">
        <link rel="stylesheet" href="{{asset('css/users/user.css')}}" media="screen">
        <!-- ========== MODERNIZR ========== -->
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{asset('js/modernizr/modernizr.min.js')}}"></script>
        <script>
            window.App = {!! json_encode([
                'user' => Auth::user(),
                'signedIn' => Auth::check(),
                'profile' => Auth::user()->profile,

                ]) !!};
        </script>
        <!-- ========== GoogleMapAPI ======= -->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCvgH8BDABIv27ogFTQ9_PYTPPdV6TfvvQ&libraries=places"></script>
    </head>
    <body class="top-navbar-fixed">
        <div id ="app" class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
            <nav class="navbar top-navbar bg-white box-shadow">
            	<div class="container-fluid">
                    <div class="row">
                        <div class="navbar-header no-padding">
                			<a class="navbar-brand" href="index.html">
                			    <img src="{{ asset('img/logo.svg') }}" alt="Junks.ng" class="logo">
                			</a>
                            <span class="small-nav-handle hidden-sm hidden-xs"><i class="fa fa-outdent"></i></span>
                			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                				<span class="sr-only">Toggle navigation</span>
                				<i class="fa fa-ellipsis-v"></i>
                			</button>
                            <button type="button" class="navbar-toggle mobile-nav-toggle" >
                				<i class="fa fa-bars"></i>
                			</button>
                		</div>
                        <!-- /.navbar-header -->

                		<div class="collapse navbar-collapse" id="navbar-collapse-1">
                			<ul class="nav navbar-nav" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                <li class="hidden-sm hidden-xs"><a href="#" class="full-screen-handle"><i class="fa fa-arrows-alt"></i></a></li>
                                <li class="hidden-sm hidden-xs"><a href="#"><i class="fa fa-search"></i></a></li>
                				<li class="hidden-xs hidden-xs"><!-- <a href="#">My Tasks</a> --></li>
                			</ul>
                            <!-- /.nav navbar-nav -->

                			<ul class="nav navbar-nav navbar-right" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                <!-- /.dropdown -->
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" 
                                    data-toggle="dropdown" aria-haspopup="true" id="dropdownMenu1" 
                                    aria-expanded="true"><i class="fa fa-bell"></i>
                                    <span class="badge badge-warning" >{{count(Auth::user()->notifications)}}</span>
                                    </a>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" >
                                            @foreach(Auth::user()->unreadNotifications as $notification)
                                            <li><a href="#">{{$notification->data['name']}} has been approved</a><span class="pull-right">
                                                <form>
                                                    <button class="btn btn-primary"><i class="fa fa-edit"></i></button>
                                                </form>
                                            </span></li>
                                            @endforeach
                                        </ul>

                                    </li>
                				<li><a href="#" class=""><i class="fa fa-comments"></i></a></li>
                				<li class="dropdown">
                					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}<span class="caret"></span></a>
                					<ul class="dropdown-menu profile-dropdown">
                						<li class="profile-menu bg-gray">
                						    <div class="">
                						        <img src="http://placehold.it/60/c2c2c2?text=User" alt="" class="img-circle profile-img">
                                                <div class="profile-name">
                                                    <h6>{{ Auth::user()->name }}</h6>
                                                    <a href="#">View Profile</a>
                                                </div>
                                                <div class="clearfix"></div>
                						    </div>
                						</li>
                						<li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                						<li><a href="#"><i class="fa fa-sliders"></i> Account Details</a></li>
                						<li role="separator" class="divider"></li>
                						<li><a href="{{ route('users.logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="color-danger text-center">
                                             <i class="fa fa-sign-out"></i> Logout</a>
                                                <form id="logout-form" action="{{ route('users.logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                             </li>
                					</ul>
                				</li>
                                <!-- /.dropdown -->
                                <li><a href="#" class="hidden-xs hidden-sm open-right-sidebar"><i class="fa fa-ellipsis-v"></i></a></li>
                			</ul>
                            <!-- /.nav navbar-nav navbar-right -->
                		</div>
                		<!-- /.navbar-collapse -->
                    </div>
                    <!-- /.row -->
            	</div>
            	<!-- /.container-fluid -->
            </nav>

            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                    @include('user.layouts.sidebar')
                    <!-- /.left-sidebar -->

                    <router-view></router-view>

                    
                    <!-- /.main-page -->

                    <div class="right-sidebar bg-white fixed-sidebar">
                        <div class="sidebar-content">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Junks.ng <i class="fa fa-times close-icon"></i></h4>
                                        <p>We are Glad to have you with us and we wish you the best cause your just the best.</p>
                                        <p>As long as your Junks are non-perishable we are sure to buy them.</p>
                                        
                                    </div>
                                    <!-- /.col-md-12 -->

                                   
                                    <!-- /.text-center -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </div>
                        <!-- /.sidebar-content -->
                    </div>
                    <!-- /.right-sidebar -->

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->

        </div>
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        {{-- <script src="{{asset('js/jquery/jquery-2.2.4.min.js')}}"></script> --}}
        <script src="{{asset('js/jquery-ui/jquery-ui.min.js')}}"></script>
        {{-- <script src="{{asset('js/bootstrap/bootstrap.min.js')}}"></script> --}}
        <script src="{{asset('js/pace/pace.min.js')}}"></script>
        <script src="{{asset('js/lobipanel/lobipanel.min.js')}}"></script>
        <script src="{{asset('js/iscroll/iscroll.js')}}"></script>
        <script src="{{asset('js/icheck/icheck.min.js')}}"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="{{asset('js/prism/prism.js')}}"></script>

        <!-- ========== THEME JS ========== -->
        <script src="{{asset('js/script.js')}}"></script>
        <script src="{{asset('js/main.js')}}"></script>
        <script>
            $(function($) {
                $('input.blue-style').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue'
                });
                $('input.green-style').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green'
                });
                $('input.red-style').iCheck({
                    checkboxClass: 'icheckbox_square-red',
                    radioClass: 'iradio_square-red'
                });
                $('input.flat-black-style').iCheck({
                    checkboxClass: 'icheckbox_flat',
                    radioClass: 'iradio_flat'
                });

                $('input.line-style').each(function(){
                    var self = $(this),
                      label = self.next(),
                      label_text = label.text();

                    label.remove();
                    self.iCheck({
                      checkboxClass: 'icheckbox_line-blue',
                      radioClass: 'iradio_line-blue',
                      insert: '<div class="icheck_line-icon"></div>' + label_text
                    });
                  });
            });
        </script>
        </script>

        <!-- ========== ADD custom.js FILE BELOW WITH YOUR CHANGES ========== -->
    </body>
</html>