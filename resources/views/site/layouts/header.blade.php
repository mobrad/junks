<div class="container">
    <div class="row">
        <div class="col-md-3 col-xs-3 col-sm-3 header_logo">
            <a href="/">
                <img src="{{ asset('img/logo.svg') }}"/>                    
            </a>
        </div>
        <div class="userbar col-md-9 col-sm-9 col-xs-9 ">
            @if(Auth::check() == true)
            <div class="dropdown mr-20">
                {{-- dropdown button --}}
                <a class="userbar_name dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                {{Auth::user()->name}} <span class="caret"></span></a>
                {{-- dropdown list --}}
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a href="{{route('users.dashboard')}}">Dashboard</a></li>
                     <li>
                        <a href="{{ route('users.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        Logout</a>
                    </li>
                    <form id="logout-form" action="{{ route('users.logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </ul>
            </div>
            @else
                <a href="{{ route('login') }}" class="userbar_link_login">Login</a>
                <a href="{{ route('register') }}" class="userbar_link_reg">Register</a>
            @endif
            <a href="{{ route('sell.junk')}}" class="userbar_link btn btn-primary">Sell your Junk</a>
        </div>
    </div>
</div>