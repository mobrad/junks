<footer> 
    <div class="container">
        <div class="row footer-link">
            <div class="col-md-3">
            <h5> What are we</h5>
            <ul>
                <li>About Us</li>
                <li>How to Sell the most Junk</li>
                <li>How we make our money</li>
            </ul>
            </div>
            <div class="col-md-3">
            <h5> Our social Links</h5>
            <ul>
                <li><a href="https://www.twitter.com">Twitter</a></li>
                <li><a href="https://www.facebook.com/junksng/">Facebook</a></li>
            </ul>
            </div>
            <div class="col-md-3">
            <h5> Contact Us</h5>
            <ul>
                <li>hello@junks.ng</li>
                <li>08118022308,09037228208</li>
                <li>Head Office : No. 22 Ikwerre Road Mile 1 Diobu Port Harcourt
                </li>
            </ul>
            </div>
             <div class="col-md-3">
            <h5> Payment Method</h5>
            <ul>
                <li><i class="fa fa-cc-visa"></i></li>
                <li><i class="fa fa-cc-mastercard"></i></li>
            </ul>
            </div>
        </div>

        <div class="row">
            <hr>
            <p class="copyright"> &copy; 2017 Junks.ng. All Rights Reserved</p>
        </div>
    </div>
</footer>
