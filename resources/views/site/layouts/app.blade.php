<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        @stack('seo')

        <title>{{config('app.name')}},we buy junks</title>

        <link rel="stylesheet" href="{{asset('css/admin/main.css')}}" media="screen">
        <link rel="stylesheet" href="{{asset('css/app.css')}}" media="screen" >
        <link rel="stylesheet" href="{{asset('css/admin/font-awesome.min.css')}}" media="screen"/>
        @stack('styles')
        <!-- ========== MODERNIZR ========== -->
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{asset('js/modernizr/modernizr.min.js')}}"></script>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCvgH8BDABIv27ogFTQ9_PYTPPdV6TfvvQ&libraries=places"></script>
        @php
            $profile = null;
            if (Auth::check() == true) {
                $profile = Auth::user()->profile;
            }
        @endphp
         <script>
             window.App = {!!
                json_encode([
                'user' => Auth::user(),
                'profile' => $profile,
                'signedIn' => Auth::check(),
                'csrfToken' => csrf_token(),
                ])
                !!};
         </script>
        <!-- Styles -->
    </head>
    <body>
        <div id="site" class="body-wrapper">
            <!-- Page caption -->
            <header :class="{shrink: overTop}" class="navbar navbar-fixed-top">
                @include('site.layouts.header')
            </header> 

            <div class="page-content">
                @yield('content')
            </div>
                <!--footer -->
            @include('site.layouts.footer')
        </div>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="{{asset('js/script.js')}}"></script>
        <script src="{{asset('js/jquery-ui/jquery-ui.min.js')}}"></script>
         <!-- ========== PAGE JS FILES ========== -->
        @stack('scripts')
        <!-- ========== THEME JS ========== -->
    </body>
</html>