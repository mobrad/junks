<div class="panel {{ isset($class) ? $class : 'junk-vertical' }}">
    <div class="panel-body" style="padding: 10px;">
        <a href="{{$junk->path()}}">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <img src="{{ Storage::url($junk->image) }}" class="img-responsive" width="150px" height="150px">
            </div>
        </a>
        <div class="col-md-8">
            <div class="row">
                <a href="{{$junk->path()}}">
                    <h3 class="title">
                        {{$junk->name}}
                        <span class="price pull-right">&#xf20A6; {{ number_format($junk->price) }}</span>
                    </h3>
                </a>
            </div>
            <div class="row">
                <div class="desc">
                    {{$junk->description}}
                    <ul class="">
                        <li>Quantity : {{$junk->quantity}}</li>
                        <li>Negitioble : @if(!$junk->pricing) Price is Negiotiable 
                            @else Price is not Negiotiable @endif
                        </li>
                    </ul>
                </div>
                <br>
            </div>

            <div class="row">
                <div class="small ">
                    <div class="list-group list-inline">
                        <li class="list-group-item">
                            <i class="fa fa-map-marker"></i>
                            {{$junk->owner->profile->address}}                                                    
                        </li>
                        <li class="list-group-item">
                            <i class="fa fa-calendar-o"></i>
                            {{$junk->created_at->toFormattedDateString()}}                                                     
                        </li>
                    </div>
                </div>
            </div>
        </div>                                 
    </div>
</div>