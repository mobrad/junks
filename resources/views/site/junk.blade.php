@extends('site.layouts.app')

@push('styles') 
        <link rel="stylesheet" href="{{asset('css/admin/animate-css/animate.min.css')}}" media="screen" >
        <link rel="stylesheet" href="{{asset('css/admin/lobipanel/lobipanel.min.css')}}" media="screen" >
        <!-- Fonts -->
        <link rel="stylesheet" href="{{asset('css/admin/icheck/skins/line/blue.css')}}" >
        <!-- ========== THEME CSS ========== -->
        <link rel="stylesheet" href="{{asset('css/admin/main.css')}}" media="screen" >
        <style>
            body {
                background-color: #ccc;
            }
        </style>
@endpush

@push('scripts')
    <!-- Scripts -->
    <script src="{{asset('js/pace/pace.min.js')}}"></script>
    <script src="{{asset('js/lobipanel/lobipanel.min.js')}}"></script>
    <script src="{{asset('js/iscroll/iscroll.js')}}"></script>

    <!-- ========== PAGE JS FILES ========== -->
    <script src="/js/icheck/icheck.min.js"></script>
    <script src="{{asset('js/junk.js')}}"></script>

    <!-- ========== THEME JS ========== -->
    <script src="{{asset('js/main.js')}}"></script>
    <script>
        $(function(){
            $('input.flat-blue-style').iCheck({
                checkboxClass: 'icheckbox_flat-blue'
            });
        });
        
    </script>
@endpush

@push('seo') 
    <meta name="description" content="{{$junk->name}}:{{$junk->description}}:{{$junk->price}}">
    <meta name="keywords" content="Junks, unused items,scraps,waste,recycle,sell trash for money,buy junks,scrap,junk,old clothes,plastic,old,electronics,recycle junks,recycle junk">
@endpush

@section('content')
    <br>
    <div class=" mt-20 junk container">
        <div class="row">
            <div class="col-md-7">
                <div class="panel junk-panel">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h2>{{$junk->name}}</h2>
                            <div class="desc clearfix">
                                <span class="ml-5"><i class="fa fa-calendar-o"></i> {{ $junk->created_at->toFormattedDateString() }} </span>
                                <span class="ml-5"><i class="fa fa-map-marker"></i> {{ $junk->owner->profile->address }} </span>
                                <span class="ml-10"><i class="fa fa-eye"></i> {{ @$junk->view->amount ? $junk->view->amount: 0 }} </span>
                             </div>
                        </div>
                    </div>
                    <div class="panel-body">
                      <div class="row">
                           <div class="col-md-12">
                                <figure class="" @click="showFigure = !showFigure" :class="{shorten : !showFigure}">
                                   <img src="{{ Storage::url($junk->image) }}" class="img-responsive"/>                                    
                                </figure>
                           </div> 
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="desc-title">Description</h4>
                                <table class="table table-condensed">
                                    <tr>
                                      <td class="active">Quantity</td>
                                      <td class="success">{{$junk->quantity}}</td>
                                    </tr>
                                    <tr>
                                      <td class="active">Negiotiable</td>
                                      <td class="success">
                                          @if($junk->pricing == 0)
                                          Price is Negiotable
                                          @else
                                          Not Negiotable
                                          @endif
                                      </td>
                                      
                                    </tr>
                                    <tr>
                                        <td class="active">Category</td>
                                        <td class="success">
                                           {{$junk->category->name}}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success"><span><i class="fa fa-phone"></i></span>080-XXX</button>
                                <div class="pull-right" style="font-size: 25px; margin: 5px;">
                                <div>
                                    <i style="color: #6e6ec7;" class="fa fa-facebook-official"></i>
                                    <i style="color: #7fc0e6;" class="fa fa-twitter-square"></i>
                                    <i class="fa fa-instagram"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="sidebar panel">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <span class="text-right">&#xf20A6; {{ number_format($junk->price) }}</span>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                           <div class="col-md-4">
                               <figure class="thumbnail user-thumbnail">
                                   <img src="http://localhost:8000/storage/{{$junk->owner->profile->image}}" class="img-responsive"/>
                                   <i class="fa fa-user" style="font-size: 3em"></i>
                               </figure>
                           </div>
                           <div class="user-info col-md-8">
                                <h4 class="user-name">{{$junk->owner->name}}</h4>
                                <div><i class="fa fa-check-circle" style="color: green"></i>
                                    Verified Agent
                                </div>
                           </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tips">
                                    <h5 class="mt-5">Read these Tips</h5>
                                    <ul class="list-group">
                                        <li><b>-</b> Make sure your account gets credited before you sell</li>
                                        <li><b>-</b> Make sure you sell the junks at a reasonable price</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <button class="btn btn-success"><span><i class="fa fa-phone"></i></span>080-XXX</button>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <h2 class="underline" style="margin: 20px;color: black;">Similar Junks</h2>
        </div>

        <div class="row">
            @foreach($junks as $junk)
                @include('site.layouts.junk_card',['junk' => $junk, 'class' => 'junk-vertical col-md-7'])
            @endforeach
        </div>
    </div>
@endsection