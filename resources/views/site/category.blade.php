@extends('site.layouts.app')
@section('content')
<div class="row">
    <div class="junks container">
        <div class="col-md-4">
            <br>
            <div inline-template is="junk-category" class="category-sidebar clearfix">
                <section>
                    <div class="col-md-12">
                        <div class="cat_nav">
                            <h3 class="classic-heading">Category</h3>
                            <ul class="nav nav-stacked">
                                @foreach($cat as $cats)
                                    @php
                                        $route = route('junk.category',['fixcat'=> str_slug($cats->name)]);
                                    @endphp
                                    <li class="{{ request()->is($route) ? 'active' : '' }}">
                                        <a href="{{ $route }}">
                                            {{$cats->name}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>                    
                    <div class="col-md-12">
                        <div class="cat_nav_filter">
                            <h3>Filters</h4>
                            <h5>PRICE</h5>
                            <form>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input id="price-range" type="text">                                    
                                        </div>
                                        <div class="form-group text-center">
                                            
                                            <input type="submit" placeholder="biscut" class="btn btn-success">
                                        </div>
                                    </div>
                                 </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <div class="col-md-8">
            <div class="main-bar">
                <div :style="{top: overTop ? scrollY - 30 + 'px' : 0 }" class="cat-header">
                    <h3 class="cat-heading classic-heading"> {{ strtolower($fixcat->name) }}</h3>
                    <ul class="nav nav-pills cat-links ">
                        <li>Newest</li>
                        <li>Cheapest</li>
                    </ul>
                    </div>
                </div>
                <div class="junk-show">
                    <div class="row">
                        <div class="col-md-12">
                            @foreach($junks as $more)
                                @include('site.layouts.junk_card',['junk' => $more, 'class' => 'junk-vertical'])
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row text-center mt-30 mb-30">
             {{$junks->links()}}            
        </div>
    </div>
</div>
@endsection