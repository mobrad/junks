@extends('site.layouts.app')
@section('content')
	<div class="container">
		<br>
		<sell-form inline-template>
			<form @submit.prevent="submitJunk" class="sell-form col-md-8 col-md-offset-2" @keydown="form.errors.clear($event.target.name )">
	            <input type="hidden" name="_token" :value="csrf"/>
	            <h5 class="underline mt-n">Fill the details about your junk</h5>
	            <div class="row">
	            	  <!-- /.col-md-6 -->
	                <div class="col-md-6">
	                    <div class="form-group">
	                        <label>Name of your Junk</label>
	                        <input v-model="form.name" type="text" name="name" class="form-control" placeholder="Enter your junk name">
	                        <span  class="help alert-warning" role="alert" v-text="form.errors.get('name')"></span>
	                    </div>

                        <label for="image" class="junk-image-holder p-15">
                        	<h1 v-if="img_src == '' "  class="underline"><i class="fa fa-camera"></i> <br/> UPLOAD <br/>JUNK<br/> IMAGE</h1>
                        	<h1 v-else class="underline"><img :src="img_src" class="img-responsive"></h1>
                            <input  style="display : none;" type="file" @change="onChange" class="form-control"  id="image" name="image">
                            <span id="img"  class="help alert-warning" role="alert" v-text="form.errors.get('image')"></span>
                        </label>
	                </div>

	                <div class="col-md-6">
	                    <div class="form-group">
	                        <label for="name13">Category of your junk</label>
	                        <select v-model="form.category" name="category" class="form-control">
	                            <option value="0" selected>Select a Category</option>
	                            <option v-for="(cats,index) in cat" :value="index">@{{ cats }}</option>
	                        </select>
	                        <span  class="help alert-warning" role="alert" v-text="form.errors.get('category')"></span>
	                    </div> 
	                    <div class="form-group">
	                        <label>Price you intend to sell your junk</label>
	                        <input type="number" v-model="form.price" class="form-control" name="price" placeholder="Enter Your Price">
	                        <span  class="help alert-warning" role="alert" v-text="form.errors.get('price')"></span>
	                    </div>
	                    <div class="form-group">
	                        <label>Contact Number</label>
	                        <input type="text" class="form-control" @input="stylizeNumber" v-model="form.telephone" name="telephone" placeholder="Enter Your Mobile Phone Number">
	                        <span  class="help alert-warning" role="alert" v-text="form.errors.get('telephone')"></span>
	                    </div> 
	                    <div class="form-group">
	                        <label>Quantity of the Junk</label>
	                        <input type="number" class="form-control" v-model="form.quantity" name="quantity" placeholder="Enter the Quantity">
	                        <span  class="help alert-warning" role="alert" v-text="form.errors.get('quantity')"></span>
	                    </div> 
	                     <div class="form-group">
	                        <label>Description of your Junk</label>
	                        <input type="text" class="form-control" v-model="form.description" name="quantity" placeholder="Enter the Description">
	                        <span  class="help alert-warning" role="alert" v-text="form.errors.get('description')"></span>
	                    </div>
	                </div>
	            </div>
	           
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input name="pricing" v-model="form.pricing" type="checkbox"> Check if its negiotiable
                        </label>
                    </div>
	            </div>

				<hr width="80%">

	            <div class="row">
                    <div class="col-md-12">
		                <h5 class="underline mt-n">Address Details</h5>              
                        <div>
	                        <vue-google-autocomplete
		                        ref="address"
		                        id="map"
		                        classname="form-control"
		                        placeholder="Please type your address"
		                        v-on:placechanged="getAddressData"
		                        country="ng"
		                        name="address"
		                        :value="form.address"
		                        >
		                        </vue-google-autocomplete>
	                        <span  class="help alert-warning" role="alert" v-text="form.errors.get('address')"></span>
	                    </div>
                    </div>
	            </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >City</label>
                            <input :disabled="filled" type="text" v-model="form.locality" class="form-control" name="locality" placeholder="Enter Your City">
                            <span  class="help alert-warning" role="alert" v-text="form.errors.get('locality')"></span>
                        </div>

                        <div class="form-group">
                            <label>State</label>
                            <input :disabled="filled" type="text" class="form-control" v-model="form.state" name="state" placeholder="Enter Your State">
                            <span  class="help alert-warning" role="alert" v-text="form.errors.get('state')"></span>
                        </div>
                    </div>
                    <!-- /.col-md-6 -->

                    <div class="col-md-6"> 
                    	<div class="form-group">
                            <label for="country">Country</label>
                            <input :disabled="filled" type="text" class="form-control" v-model="form.country" name="country"  placeholder="Enter Your Country">
                            <span  class="help alert-warning" role="alert" v-text="form.errors.get('country')"></span>
                        </div>
                        <label>Zip Code</label>
                        <input type="text" class="form-control" v-model="form.zip" name="zip"  placeholder="Enter Your Zip Code">
                        <span  class="help alert-warning" role="alert" v-text="form.errors.get('zip')"></span>
                    </div>
                </div>

                <div class="form-group text-left">
                    <div class="btn-group mt-10" role="group">
                       <button type="submit" class="btn btn-default">
                       		<i class="fa fa-paper-plane"></i> Submit<i v-show="loader" class="fa fa-spinner fa-spin"></i>
                       </button>
                    </div>
                </div>
	        </form>
		</sell-form>
	</div>
@endsection
                
 