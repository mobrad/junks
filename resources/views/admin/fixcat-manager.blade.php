<div class="main-page">
    <div class="container-fluid">
        <div class="row page-title-div">
            <div class="col-md-6">
                <h2 class="title">Smart Fix Category</h2>
            </div>
            <!-- /.col-md-6 -->
            <!-- /.col-md-6 text-right -->
        </div>
        <!-- /.row -->
        <div class="row breadcrumb-div">
            <div class="col-md-6">
                <ul class="breadcrumb">
					<li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
					<li><a href="#">dashboard</a></li>
					<li class="active">FixCat Manager</li>
				</ul>
            </div>
            <!-- /.col-md-6 -->
            <div class="col-md-6 text-right">
                <a href="#"><i class="fa fa-comments"></i> Talk to us</a>
                <a href="#" class="pl-20"><i class="fa fa-cog"></i> Settings</a>
            </div>
            <!-- /.col-md-6 -->
        </div>
                            <!-- /.row -->
    </div>
                        <!-- /.container-fluid -->

    <section class="section">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h5 >Add a category to the Smart Fix</h5>
                            </div>
                        </div>
                        <div class="panel-body">

                            <form id="catAdd" method="POST" action="{{route('fixcat.store')}}">
                            {{ csrf_field() }}
                                <div class="form-group">
                            		<label for="name">Smart Fix Category name</label>
                            		<input type="text" name ="name" class="form-control" id="name" placeholder="Category name">
                            	</div>
                            

                            	<button id="cli" type="submit" class="btn btn-primary">Add</button>
                            </form>

                            
                            <!-- /.col-md-12 -->
                        </div>
                    </div>
                </div>
                <!-- /.col-md-6 -->
                    
                
                <!-- /.col-md-6 -->
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h5>Available Category</h5>
                            </div>
                        </div>
                        <div class="panel-body">

                            <div class="list-group">
                            <li class="list-group-item">Cras justo odio</li>
                                
                                
                            </div>

                           
                            <!-- /.col-md-12 -->
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container-fluid -->
    </section>
</div>
