<div class="left-sidebar fixed-sidebar bg-black-300 box-shadow">
                        <div class="sidebar-content">
                            
                            <!-- /.user-info -->

                            <div class="sidebar-nav">
                                <ul class="side-nav color-gray">
                                    
                                    <li class="nav-header">
                                        <span class="">Main Category</span>
                                    </li>
                                    <router-link to="/" tag="li" class="has-children" exact>
                                        <a><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
                                        
                                    </router-link>
                                    <li class="nav-header">
                                        <span class="">Users</span>
                                    </li>
                                    
                                   
                                    <router-link tag="li" to="/all-users"><a href="color-primary.html"><i class="fa fa-bank"></i> <span>Registered Users</span></a></router-link>
                                                
                                    <router-link tag="li" to="/fixcat" exact><a><i class="fa fa-newspaper-o "></i> <span>Manage Category</span></a></router-link>
                                    
                                    </li>
                                     <li class="has-children">
                                        <a href="#"><i class="fa fa-user"></i> <span>Admins</span> <i class="fa fa-angle-right arrow"></i></a>
                                        <ul class="child-nav">
                                            <router-link tag="li" to="/manage-admins"><a><i class="fa fa-lock"></i> <span>Manage Admins</span></a></router-link>
                                            <router-link tag="li" to="/admin-activity"><a><i class="fa fa-list"></i> <span>Admin activity</span></a></router-link>
                                            <router-link tag="li" to="/profile"><a><i class="fa fa-table"></i> <span>Profile</span></a></router-link>
                                        </ul>
                                    </li>
                                    <router-link tag="li" to="/comments"><a><i class="fa fa-comments-o"></i> <span>Comments</span></a></router-link>
                                    <router-link tag ="li" to="/reviews"><a><i class="fa fa-area-chart"></i> <span>Reviews</span></a></router-link>
                                    <router-link tag="li" to="/partners"><a><i class="fa fa-pencil"></i> <span>Partners</span></a></router-link>
                                    <router-link tag="li" to="/clients"><a><i class="fa fa-server"></i> <span>Clients</span></a></router-link>
                                    <router-link tag="li" to="/testimonials"><a><i class="fa fa-bullseye"></i> <span>Testimonials</span></a></router-link>
                                </ul>
                                <!-- /.side-nav -->
                                <div class="purchase-btn hidden-sm hidden-xs">
                                    <a href="/" target="_blank" class="btn btn-success btn-labeled">View Home Page<span class="btn-label btn-label-right"><i class="fa fa-check"></i></span></a>
                                </div>
                            </div>
                            <!-- /.sidebar-nav -->
                        </div>
                        <!-- /.sidebar-content -->
                    </div>