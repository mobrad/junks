<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Junks-Admin</title>

        <!-- ========== COMMON STYLES ========== -->
        <link rel="stylesheet" href="{{asset('css/admin/bootstrap.min.css')}}" media="screen" >
        <link rel="stylesheet" href="{{asset('css/admin/font-awesome.min.css')}}" media="screen" >
        <link rel="stylesheet" href="{{asset('css/admin/animate-css/animate.min.css')}}" media="screen" >
        <link rel="stylesheet" href="{{asset('css/admin/lobipanel/lobipanel.min.css')}}" media="screen" >

        <!-- ========== PAGE STYLES ========== -->
        <link rel="stylesheet" href="{{asset('css/admin/prism/prism.css')}}" media="screen" > <!-- USED FOR DEMO HELP - YOU CAN REMOVE IT -->
        <link rel="stylesheet" href="{{asset('css/admin/icheck/skins/line/blue.css')}}" >
        <link rel="stylesheet" href="{{asset('css/admin/icheck/skins/line/red.css')}}" >
        <link rel="stylesheet" href="{{asset('css/admin/icheck/skins/line/green.css')}}" >

        <!-- ========== THEME CSS ========== -->
        <link rel="stylesheet" href="{{asset('css/admin/main.css')}}" media="screen" >

        <!-- ========== MODERNIZR ========== -->

        <script src="{{asset('js/app.js')}}"></script>
        <script src="{{asset('js/modernizr/modernizr.min.js')}}"></script>
        <script>
            window.App = {!! json_encode([
                'user' => Auth::user(),
                'signedIn' => Auth::check(),
                'csrfToken' => csrf_token(),
                ]) !!};
        </script>
    </head>
    <body class="top-navbar-fixed">
        <div id ="admin" class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
            <nav class="navbar top-navbar bg-white box-shadow">
            	<div class="container-fluid">
                    <div class="row">
                        <div class="navbar-header no-padding">
                			<a class="navbar-brand" href="index.html">
                			    <img src="images/logo-dark.svg" alt="Junk Admin" class="logo">
                			</a>
                            <span class="small-nav-handle hidden-sm hidden-xs"><i class="fa fa-outdent"></i></span>
                			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                				<span class="sr-only">Toggle navigation</span>
                				<i class="fa fa-ellipsis-v"></i>
                			</button>
                            <button type="button" class="navbar-toggle mobile-nav-toggle" >
                				<i class="fa fa-bars"></i>
                			</button>
                		</div>
                        <!-- /.navbar-header -->

                		<div class="collapse navbar-collapse" id="navbar-collapse-1">
                			<ul class="nav navbar-nav" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                <li class="hidden-sm hidden-xs"><a href="#" class="full-screen-handle"><i class="fa fa-arrows-alt"></i></a></li>
                                <li class="hidden-sm hidden-xs"><a href="#"><i class="fa fa-search"></i></a></li>
                				<li class="hidden-xs hidden-xs"><!-- <a href="#">My Tasks</a> --></li>
                			</ul>
                            <!-- /.nav navbar-nav -->

                			<ul 
                            class="nav navbar-nav navbar-right" 
                            data-dropdown-in="fadeIn" 
                            data-dropdown-out="fadeOut">
                                <!-- /.dropdown -->
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" 
                                    data-toggle="dropdown" aria-haspopup="true" id="dropdownMenu1" 
                                    aria-expanded="true"><i class="fa fa-bell"></i>
                                    <span class="badge badge-warning" >{{count(Auth::user()->unreadNotifications )}}</span>
                                    </a>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" >
                                            @foreach(Auth::user()->unreadNotifications as $notification)
                                            <li>
                                                <a href="#"> {{$notification->data['name']}} Just Registered 
                                                    <span class="clearfix">
                                                        <button class="btn btn-primary pull-right"><i class="fa fa-edit"></i></button>
                                                    </span>
                                                </a>
                                               
                                            </li>
                                            @endforeach
                                        </ul>

                                    </li>
                				<li><a href="#" class=""><i class="fa fa-comments"></i></a></li>
                				<li class="dropdown">
                					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}<span class="caret"></span></a>
                					<ul class="dropdown-menu profile-dropdown">
                						<li class="profile-menu bg-gray">
                						    <div class="">
                						        <img src="http://placehold.it/60/c2c2c2?text=User" alt="" class="img-circle profile-img">
                                                <div class="profile-name">
                                                    <h6>{{ Auth::user()->name }}</h6>
                                                    <a href="#">View Profile</a>
                                                </div>
                                                <div class="clearfix"></div>
                						    </div>
                						</li>
                						<li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                						<li><a href="#"><i class="fa fa-sliders"></i> Account Details</a></li>
                						<li role="separator" class="divider"></li>
                						<li><a href="{{ route('admin.logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="color-danger text-center">
                                             <i class="fa fa-sign-out"></i> Logout</a>
                                                <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                             </li>
                					</ul>
                				</li>
                                <!-- /.dropdown -->
                                <li><a href="#" class="hidden-xs hidden-sm open-right-sidebar"><i class="fa fa-ellipsis-v"></i></a></li>
                			</ul>
                            <!-- /.nav navbar-nav navbar-right -->
                		</div>
                		<!-- /.navbar-collapse -->
                    </div>
                    <!-- /.row -->
            	</div>
            	<!-- /.container-fluid -->
            </nav>

            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                    @include('admin.layouts.sidebar')
                    <!-- /.left-sidebar -->

                    <transition>
                      <keep-alive>
                        <router-view></router-view>
                      </keep-alive>
                    </transition>

                    
                    <!-- /.main-page -->

                    <div class="right-sidebar bg-white fixed-sidebar">
                        <div class="sidebar-content">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Useful Sidebar <i class="fa fa-times close-icon"></i></h4>
                                        <p>Code for help is added within the main page. Check for code below the example.</p>
                                        <p>You can use this sidebar to help your end-users. You can enter any HTML in this sidebar.</p>
                                        <p>This sidebar can be a 'fixed to top' or you can unpin it to scroll with main page.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    </div>
                                    <!-- /.col-md-12 -->

                                    <div class="text-center mt-20">
                                        <button type="button" class="btn btn-success btn-labeled">Purchase Now<span class="btn-label btn-label-right"><i class="fa fa-check"></i></span></button>
                                    </div>
                                    <!-- /.text-center -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </div>
                        <!-- /.sidebar-content -->
                    </div>
                    <!-- /.right-sidebar -->

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->

        </div>
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="{{asset('js/jquery-ui/jquery-ui.min.js')}}"></script>
        <script src="{{asset('js/pace/pace.min.js')}}"></script>
        <script src="{{asset('js/lobipanel/lobipanel.min.js')}}"></script>
        <script src="{{asset('js/iscroll/iscroll.js')}}"></script>
        <script src="{{asset('js/socket.io.js')}}"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="{{asset('js/prism/prism.js')}}"></script>

        <!-- ========== THEME JS ========== -->
        <script src="{{asset('js/main.js')}}"></script>
        <script src="{{asset('js/script.js')}}"></script>
        <script>
            $(function(){

            });
        </script>

        <!-- ========== ADD custom.js FILE BELOW WITH YOUR CHANGES ========== -->
    </body>
</html>