
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{config('app.name')}} | Login</title>

        <!-- ========== COMMON STYLES ========== -->
        <link rel="stylesheet" href="{{asset('css/admin/bootstrap.min.css')}}" media="screen" >
        <link rel="stylesheet" href="{{asset('css/admin/font-awesome.min.css')}}" media="screen" >
        <link rel="stylesheet" href="{{asset('css/admin/animate-css/animate.min.css')}}" media="screen" >

        <!-- ========== PAGE STYLES ========== -->
        <link rel="stylesheet" href="{{asset('css/admin/icheck/skins/flat/blue.css')}}" >

        <!-- ========== THEME CSS ========== -->
        <link rel="stylesheet" href="{{asset('css/admin/main.css')}}" media="screen" >

        <!-- ========== MODERNIZR ========== -->
        <script src="{{asset('js/modernizr/modernizr.min.js')}}"></script>
    </head>
    <body class="">
        <div class="main-wrapper">

            <div class="login-bg-color">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="panel login-box">
                            <div class="panel-heading">
                                <div class="panel-title text-center">
                                    <h4>{{config('app.name')}} Login</h4>
                                </div>
                            </div>
                            <div class="panel-body p-20">

                                <form method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email">Email address</label>
                                        <input type="email" class="form-control" placeholder="Enter Your Email Id" name="email" value="{{ old('email') }}" required autofocus >
                                         @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control"  placeholder="Password" name="password" required>
                                         @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="checkbox op-check">
                                        <label>
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} class="flat-blue-style"> <span class="ml-10">Remember me</span>
                                        </label>
                                    </div>
                                    <div class="form-group mt-20">
                                        <div class="">
                                        <a href="{{ route('password.request') }}" class="form-link"><small class="muted-text">Forgot Password?</small></a><br>
                                            <a href="{{ route('register') }}" class="form-link"><small class="muted-text">Not Registered?</small></a>
                                            <button type="submit" class="btn btn-success btn-labeled pull-right">Sign in<span class="btn-label btn-label-right"><i class="fa fa-check"></i></span></button>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                     <br />
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-3">
                                          <a href="{{url('/redirect')}}"><button type="button" class="btn btn-primary bg-primary-600 btn-labeled">Facebook<span class="btn-label btn-label-right"><i class="fa fa-facebook"></i></span></button></a>
                                        </div>
                                    </div> 
                                </form>
                                <hr>



                                
                                <!-- /.text-center -->

                            
                                <!-- /.text-center -->

                            </div>
                        </div>
                        <!-- /.panel -->
                        <p class="text-muted text-center"><small>Copyright © Junks.ng 2017</small></p>
                    </div>
                    <!-- /.col-md-6 col-md-offset-3 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /. -->

        </div>
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="{{asset('js/jquery/jquery-2.2.4.min.js')}}"></script>
        <script src="{{asset('js/jquery-ui/jquery-ui.min.js')}}"></script>
        <script src="{{asset('js/bootstrap/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/pace/pace.min.js')}}"></script>
        <script src="{{asset('js/lobipanel/lobipanel.min.js')}}"></script>
        <script src="{{asset('js/iscroll/iscroll.js')}}"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="{{asset('js/icheck/icheck.min.js')}}"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function(){
                $('input.flat-blue-style').iCheck({
                    checkboxClass: 'icheckbox_flat-blue'
                });
            });
        </script>

        <!-- ========== ADD custom.js FILE BELOW WITH YOUR CHANGES ========== -->
    </body>
</html>
