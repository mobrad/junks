<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{config('app.name')}} | Register</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     <!-- ========== COMMON STYLES ========== -->
    <link rel="stylesheet" href="{{asset('css/admin/bootstrap.min.css')}}" media="screen" >
    <link rel="stylesheet" href="{{asset('css/admin/font-awesome.min.css')}}" media="screen" >
    <link rel="stylesheet" href="{{asset('css/admin/animate-css/animate.min.css')}}" media="screen" >

    <!-- ========== PAGE STYLES ========== -->
    <link rel="stylesheet" href="{{asset('css/admin/icheck/skins/line/blue.css')}}" >
    <link rel="stylesheet" href="{{asset('css/admin/icheck/skins/flat/blue.css')}}" >

    <!-- ========== THEME CSS ========== -->
    <link rel="stylesheet" href="{{asset('css/admin/main.css')}}" media="screen" >

    <!-- ========== MODERNIZR ========== -->
</head>
<style>
    .login-bg-color .login-box {
    color: #292929 !important;
    padding: 20px;
    margin-top: 125px;
    border-radius: 4px;
    box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.4);
}
</style>
<body>
 <div class="main-wrapper">

            <div class="login-bg-color">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="panel login-box">
                            <div class="panel-heading">
                                <div class="panel-title text-center">
                                    <h4>{{config('app.name')}} Register</h4>
                                </div>
                            </div>
                            <div class="panel-body p-20">

                                <div class="section-title">
                                    <p class="sub-title text-muted text-center">Register to sell your Junks</p>
                                </div>

                                 <form  method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name">Full Name</label>
                                        <input type="text" class="form-control" id="name" placeholder="Enter Your Full Name" name="name" value="{{ old('name') }}" required autofocus >
                                         @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label>Email Address</label>
                                        <input type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Enter Your Email Id" >
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}" >
                                        <label >Password</label>
                                        <input type="password" class="form-control" name="password" required placeholder="Password">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group" >
                                        <label >Confirm Password</label>
                                        <input type="password" class="form-control" name="password_confirmation" required placeholder="Password">
                                        
                                    </div>
                                    <div class="form-group mt-20">
                                        <div class="">
                                            <a href="{{route('login')}}" class="form-link"><small class="muted-text">Already Registered?</small></a>
                                            <button type="submit" class="btn btn-success btn-labeled pull-right">Register<span class="btn-label btn-label-right"><i class="fa fa-check"></i></span></button>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </form>

                                <hr>

                                <h5 class="text-center mt-10 mb-20">or register with</h5>

                                <div class="text-center">
                                   <a style="color: white" href="{{url('/redirect')}}"> <button type="button" class="btn btn-primary bg-primary-600 btn-labeled">Facebook<span class="btn-label btn-label-right"><i class="fa fa-facebook"></i></span></button></a>
                                </div>
                                <!-- /.text-center -->

                            </div>
                        </div>
                        <!-- /.panel -->
                        <p class="text-muted text-center"><small>Copyright © Junks 2017</small></p>
                    </div>
                    <!-- /.col-md-6 col-md-offset-3 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /. -->

        </div>
        <!-- /.main-wrapper -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{asset('js/jquery/jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset('js/jquery-ui/jquery-ui.min.js')}}"></script>
    <script src="{{asset('js/bootstrap/bootstrap.min.js')}}"></script>

    <!-- ========== PAGE JS FILES ========== -->
    <script src="{{asset('js/icheck/icheck.min.js')}}"></script>

    <!-- ========== THEME JS ========== -->
    <script src="{{asset('js/main.js')}}"></script>
    <script>
        $(function(){
            $('input.flat-blue-style').iCheck({
                checkboxClass: 'icheckbox_flat-blue'
            });
        });
    </script>

</body>
</html>
