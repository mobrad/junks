<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     <!-- ========== COMMON STYLES ========== -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}" media="screen" >
    <link rel="stylesheet" href="{{asset('css/admin/font-awesome.min.css')}}" media="screen" >
    <link rel="stylesheet" href="{{asset('css/admin/animate-css/animate.min.css')}}" media="screen" >
    <link rel="stylesheet" href="{{asset('css/admin/lobipanel/lobipanel.min.css')}}" media="screen" >

    <!-- ========== PAGE STYLES ========== -->
    <link rel="stylesheet" href="{{asset('css/admin/prism/prism.css')}}" media="screen" > <!-- USED FOR DEMO HELP - YOU CAN REMOVE IT -->
    <link rel="stylesheet" href="{{asset('css/admin/icheck/skins/line/blue.css')}}" >

    <!-- ========== THEME CSS ========== -->
    <link rel="stylesheet" href="{{asset('css/admin/main.css')}}" media="screen" >

    <!-- ========== MODERNIZR ========== -->
    <script src="{{asset('js/modernizr/modernizr.min.js')}}"></script>
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'SmartFix') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}  <span class="caret"></span></a>
                                    <ul class="dropdown-menu profile-dropdown">
                                        <li class="profile-menu bg-gray">
                                            <div class="">
                                                <img src="http://placehold.it/60/c2c2c2?text=User" alt="" class="img-circle profile-img">
                                                <div class="profile-name">
                                                    <h6>{{ Auth::user()->name }}</h6>
                                                    <a href="#">View Profile</a>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </li>
                                        <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                                        <li><a href="#"><i class="fa fa-sliders"></i> Account Details</a></li>
                                        <li role="separator" class="divider"></li>
                                         <li class="color-danger text-center">
                                        <a href="{{ route('users.logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('users.logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                    </ul>
                                </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{asset('js/jquery/jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset('js/jquery-ui/jquery-ui.min.js')}}"></script>
    <script src="{{asset('js/bootstrap/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/pace/pace.min.js')}}"></script>
    <script src="{{asset('js/lobipanel/lobipanel.min.js')}}"></script>
    <script src="{{asset('js/iscroll/iscroll.js')}}"></script>

    <!-- ========== PAGE JS FILES ========== -->
    <script src="js/icheck/icheck.min.js"></script>

    <!-- ========== THEME JS ========== -->
    <script src="{{asset('js/main.js')}}"></script>
    <script>
        $(function(){
            $('input.flat-blue-style').iCheck({
                checkboxClass: 'icheckbox_flat-blue'
            });
        });
    </script>

</body>
</html>
