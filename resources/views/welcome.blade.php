<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Junks| cash you Trash</title>
        <link rel="stylesheet" href="{{asset('css/admin/main.css')}}" media="screen" >
        <link rel="stylesheet" href="{{asset('css/app.css')}}" media="screen" >
        <link rel="stylesheet" href="{{asset('css/admin/font-awesome.min.css')}}" media="screen" >
        <link rel="stylesheet" href="{{asset('css/admin/animate-css/animate.min.css')}}" media="screen" 

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">


        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{asset('js/modernizr/modernizr.min.js')}}"></script>
    </head>
    <style>
        #change{
            text-align: center;
            background: #297373;
        }
        #myTopNav > ul > li > a{
            color:white;
        }
        @media(min-width: 1000px){
            #myTopNav > ul {
                margin-left:20%;
            }
        }
    </style>
    <body>
        <div id="site" class="body-wrapper">
            <!-- Page caption -->
            <div class="header-block">
                <header>
                    @include('site.layouts.header')
                </header>

                <div class="header-block-title">
                    <h1>Why throw your junks when you can sell them</h1>
                    <div class="header-block-bottom">
                        <div class="container">
                            <search></search>
                        </div>
                    </div>
                </div>
            </div>

            <div id="change" :class="{'navbar-fixed-top': passed }" class="navbar navbar-default">
               <div class="container">
                   <div class="navbar-header">
                       <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myTopNav">
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                       </button>
                   </div>
                   <div class="collapse navbar-collapse" id="myTopNav">
                   <ul class="nav navbar-nav text-center">
                       @foreach($cat as $cats)
                           <li class="nav_list_item dropdown">
                                <a href="{{route('junk.category',['fixcat'=> str_slug($cats->name)])}}">
                                    {{$cats->name}}
                                </a>
                            </li>
                       @endforeach
                   </ul>
                   </div>
               </div>
            </div>

            <div  class="page-content">
                <div class="junks container">
                    <div class="row">
                        
                    </div>
                    <div class="row junk-title">
                        <h1>Sold Junks</h1>
                    </div>

                    <div class="row">
                        @php
                        $junks = $junk->chunk(4);
                        @endphp

                        @foreach($junks as $junksChunk)
                            <div class="row">
                            @foreach($junksChunk as $junks)
                                <section class="col-md-3 col-xs-6 col-sm-6">
                                    
                                    <div  is="junk-card" :id="{{ $junks->id }}" inline-template>
                                        <div class="junk-product">
                                            <a id="some-link" href="{{$junks->path()}}" >
                                                
                                                <div class="junks-image"> 
                                                    <img class="img-responsive" src="{{ Storage::url("{$junks->image }")}}">
                                                </div>
                                                <div class="junks-desc">
                                                   <p class="title">{{$junks->name}}</p>
                                                   <span class="price">N{{$junks->price}},000</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </section>
                            @endforeach
                            </div>
                    @endforeach
                    </div>

                    <div class="row">
                        <div class="container">
                            <div class="junk-title">{{$junk->links()}}</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="container">
                            <div class="col-md-10 col-md-offset-1">
                            <h1 class="text-center">We buy your Junks</h1>
                            <p class="about">With so many ways to obtain scrap metal, plastic, old electronics it's hard to believe there's not an easy alternative for getting rid of it. </p>
                            <p class="about">When you have a collection sitting around your home, office, or job site, what can you do?</p>
                            <p class="about">Log on to Junks.ng Sell it or just call us and we would be there to clean up your junks and also pay for it </p>
                            <p class="about">We plan for a neater and greener nigeria by buying your junks, plastics, metals, aluminuim,clothes,shoes</p>
                            <p class="about">What do we do with your old clothes,bags and shoes?</p>
                            <p class="about">Well we give them to the orphanages and people who are in there need of them. <b>Cash your Trash today</b></p>
                            </div>
                            
                        </div>
                    </div>
                </div>
        <!-- end of page caption -->
                <hr>
                @include('site.layouts.footer')
            </div> 
        </div>
            
        {{-- <script src="{{asset('js/iscroll/iscroll.js')}}"></script> --}}
         <script src="{{asset('js/script.js')}}"></script>
        <script src="{{asset('js/jquery-ui/jquery-ui.min.js')}}"></script>
         <!-- ========== PAGE JS FILES ========== -->
        @stack('scripts')
        @stack('scripts')
    </body>
</html>
