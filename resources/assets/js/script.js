import userRoute from './userRoute';
import adminRoute from './adminRoute';
import VueRouter from 'vue-router';
import Form from './form';
require('bootstrap-notify');

window.swal = require('sweetalert');

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

// let token = document.head.querySelector('meta[name="csrf-token"]');

// if (token) {
//     window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
// } else {
//     console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
// }
window.Socket = require('./socket');
window.events = new Vue();
window.flash = function(message) {
    window.events.$emit('flash',message);
}

window.Form = Form;
window.Errors = require('./errors');

Vue.use(VueRouter);
//sidebar loaders

Vue.component('sell-form', require('./views/site/sell-form.vue'));
Vue.component('notification',require('./views/admin/notification/notification.vue'));
Vue.component('search',require('./views/site/components/search'));
// var socket = io('http://127.0.0.1:3000');
Vue.component('junk-category',{
    data() {
        return {
            current: 0,
        }
    },
    mounted() {

        //activates the price range in every page
        $("#price-range").slider({ min: 200, max: 500000, value: [20000, 150000], focus: true });

        var _vue = this;
        $(this.$el).find('li').click(function(){
            $(this).addClass('active').siblings().removeClass('active');
            _vue.current = $(this).index();
        });
    },
    methods: {
        fetch(object) {
            console.log(object);
            console.log('fetching');
        }
    }
});
Vue.component('junk-card', {
    props: ['id'],
    data() {
        return {
            count : 1
        }
    },
    methods : {
        submitView(){
            $('#submit-view').submit(this.processView());
        },
        processView(){
            let view = document.getElementById('submit-view');
            let formdata = new FormData(view);
            
            axios.post('/junk/' + this.id + '/view',formdata)
                .then(() => {
                    window.location = "/junk/" + this.id;
                });
        },
        unbindStop(){
            $('a').unbind('click');
        }
    }
});

// var socket = io('http://127.0.0.1:3000');
var store = localStorage;

//site root vue
if($('#site')) {

    const Site = new Vue({
        el: '#site',
        data : {
            id : 1,
            scrollY: 0,
            coverOffset: 0,
            showFigure: false,
            scrollOffset: 80,
        },
        mounted() {
            if($('#navigation')[0]) 
                this.coverOffset = $('#navigation').offset().top;

            $(window).scroll(()=> {

                if($(window).width() > 608) {   
                    this.scrollY = $(window).scrollTop();
                    this.parralax();
                }
            });
        },
        computed: {
            overTop() {
                 return this.scrollY > this.scrollOffset;
            },
            passed() {
               return this.scrollY > this.coverOffset;
            },
        },
        methods : {
            parralax() {
                // var top =  this.scrollTop + 300,
                //     _header = $('.header-block');
                // var play = () => { _header.css({backgroundPositionY: '-'+ (top / 3) +'px'}, 100, 'swing');};
                // var stopSlide = () => { };
                // (top < _header.height()) ? play() : stopSlide(); 
            }
        }
    });

}

//user root vue
if($('#app')[0]){
 
    const UsersVue = new Vue({
        el: '#app',
        router: userRoute,
        activeClass : 'active',
        data : {
            message: '',
            notification: 1,
            users :[],
            formClass: window.Form
        },
        mounted :function(){
            this.users.push(store.getItem('notify'));
            console.log(store.getItem('notify'));
        },
        created: function(){
            //listen for websockets on this channel when User event is fired
            socket.on('user-channel:App\\Events\\UserEvent',function(data){
                console.log(data.user.name);
                this.users.push(data.user.name + "Just Registered");
                store.setItem('notify', [data.user.name]);
                
            }.bind(this)); 
             
        },
        methods : {
        	dontSubmit: function(){
        		alert('biscuit');
        	}
        }
    });
}

//admin root vue
if($('#admin')[0]) {
    new Vue({
        el : '#admin',
        router : adminRoute
    });
}
