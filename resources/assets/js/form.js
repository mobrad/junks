import Errors from './errors.js';
let image = null;
class Form{
	constructor(data){
		this.originalData = data;

		for(let field in data){
			this[field] = data[field];
		}
		this.errors = new Errors();
	}
	data(){
		
		let data = Object.assign({},this);
		let formData = new FormData('client-form');

		delete data.originalData;
		delete data.errors;

		for(let i in data) {
			formData.append(i, data[i]);
		}

		return formData;

	}
	persist(image){
		  this.image = image;
		  let realImage = this.image;
		  console.log(this.data());
	}

	reset(){
		for(let field in this.originalData){
			this[field] = '';
		}
			this.errors.clear();
	}

	post(url){
		return this.submit('post',url);
	}

	delete(url){
		return this.submit('delete',url);
	}
	patch(url){
		return this.submit('patch',url);
	}


	submit(requestType,url) {
		return new Promise((resolve,reject) =>{
			axios[requestType](url, this.data())
		.then(response => {
			this.onSuccess(response.data);
			resolve(response.data);
		})
		.catch(error => {
			this.onFail(error.response.data);
			reject(error.response.data);	

			})
		

		});
		
	}
	onSuccess(data){
		
		this.reset();
		
	}
	onFail(errors){
		this.errors.record(errors);

	}
}

export default Form