require('./bootstrap');
require('bootstrap-notify');
require('bootstrap-slider');

window.swal = require('sweetalert');
window.$ = require('jquery');
window.Vue = require('vue');
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
