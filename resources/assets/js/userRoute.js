import VueRouter from 'vue-router';
let routes = [
    {
        path:'/',
        component:require('./views/Userdashboard/user-profile'),
        name: 'user-profile'
    },
    {
        path:'/junks',
        component: require('./views/Userdashboard/junks/junks'),
        name: 'junks'
    },
     {
        path:'/instructions',
        component: require('./views/Userdashboard/instruction'),
        name: 'instruction'
    }
    
    
];

export default new VueRouter({
    routes,
    base:'user',
    linkActiveClass: 'active'
});