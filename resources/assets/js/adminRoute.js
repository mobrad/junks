import VueRouter from 'vue-router';

let routes = [
    {
        path:'/',
        component: require('./views/dashboard')
    },
    {
        path:'/fixcat',
        components: {
            default : require('./views/fixcat'),
            editor : require('./views/editor'),
            flash : require('./views/flash')

        }
    },
    {
        path:'/clients',
        component: require('./views/clients/clients')
    },
    {
        path: '/partners',
        component: require('./views/partners/partners')
    },
    {
        path: '/testimonials',
        component : require('./views/testimonial/testimonial')
    }, 
];


export default new VueRouter({
    routes,
    base:'admin',
    linkActiveClass: 'active'
});

