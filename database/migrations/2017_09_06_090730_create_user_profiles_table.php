<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('image')->nullable();
            $table->string('address')->nullable();
            $table->integer('telephone')->nullable();
            $table->string('state')->nullable();
            $table->enum('gender',['M','F'])->nullable();
            $table->string('locality')->nullable();
            $table->string('country')->nullable();
            $table->integer('zip')->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
