<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\User\Junk::class,function (Faker\Generator $faker){
	return [
	'users_id' => function(){
	    $user = factory('App\User');
	    return $user;
    },
	'name' => $faker->name,
	'price' => $faker->randomDigit(),
	'image' => 'junks/junk.jpg',
	'description' => $faker->sentence(),
	'pricing' => $faker->boolean(),
	'availibity' => $faker->boolean(),
	'quantity' => $faker->randomDigit(),
	'cat_id' => 2
	];
});
