<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile(){
        return $this->hasOne('App\User\UserProfile','user_id');
    }

    public function campers(){
        return $this->hasMany('App\User\Campers','users_id');
    }
    public function registeration(){
        return $this->hasOne('App\User\Registeration','users_id');
    }
    public function province(){
        return $this->registeration->belongsTo('App\Admin\Province','province_id');
    }
}
