<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class FixCat extends Model
{
    protected $fillable = ['name'];
}
