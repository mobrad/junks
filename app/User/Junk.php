<?php

namespace App\User;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Junk extends Model
{
    use Searchable;
	protected $fillable = ['availibity'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'name';
    }
    public function owner(){
    	return $this->belongsTo('App\User','users_id');
    }
    public function path(){
        return '/junk/'.$this->category->name.'/'.str_replace(" ",'%20',$this->name);
    }
    public function category(){
    	return $this->belongsTo('App\Admin\FixCat','cat_id');
    }
    public function view(){
    	return $this->hasOne('App\User\View', 'junks_id');
    }
}
