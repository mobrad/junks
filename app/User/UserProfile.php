<?php

namespace App\User;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $fillable = ['image'];
    public $timestamps = false;
}
