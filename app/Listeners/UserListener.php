<?php

namespace App\Listeners;

use App\Events\UserEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
class UserListener
{
    use SerializesModels;
    /**
     * Create the event listener.
     *
     * @return void
     */


    public function __construct()
    {
        //
        
    }

    /**
     * Handle the event.
     *
     * @param  UserEvent  $event
     * @return void
     */
    public function handle(UserEvent $event)
    {
        var_dump($event->user->name." just registered to smartFix");
        
    }
}
