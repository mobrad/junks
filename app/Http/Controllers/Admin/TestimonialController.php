<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Testimonial;

class TestimonialController extends Controller
{
    public function __construct(){

	}

   public function index(){
   	$testimonials = Testimonial::all();
   	return $testimonials;
   }
   public function store(){
   	$this->validate(request(),[
   		'name' => 'required',
   		'image' => 'required',
   		'testimony' => 'required'
   		]);
   	$testimonials = Testimonial::forceCreate([
		'name' =>request('name'),
		'image' =>request()->file('image')->store('testimonials','public'),
		'testimony' => request('testimony')
   		]);
 	if(request()->expectsJson()){
 		return $testimonials;
 	}
  	return['message' => 'Created'];
   }
   public function update(){

   	dd(request()->address);
   }
   public function destroy($id){
   	$client = Testimonial::find($id);
   	$client->delete();
   	if(request()->expectsJson()){

                return response(['clients' => 'Testifier deleted']);
      }
   }
}
