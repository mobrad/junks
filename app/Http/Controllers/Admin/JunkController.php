<?php

namespace App\Http\Controllers\Admin;
use App\Notifications\JunkApproved;
use App\User\Junk;
use App\Admin\FixCat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JunkController extends Controller
{
    public function __construct(){

    }

    public function index(){
    	$junk = Junk::with(['category','owner'])->get();

    	return response()->json(compact('junk'));
    }
    public function status($id){
    	$junk = Junk::find($id);
    	$junk->update([
    		'availibity' => request('status'),
    		]);
        $owner = $junk->owner;
        $owner->notify(new JunkApproved($junk));
    	return redirect()->back();
    }

}
