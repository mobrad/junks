<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Partners;

class PartnerController extends Controller
{
    public function __construct(){

	}

   public function index(){
   	$partners = Partners::all();
   	return $partners;
   }
   public function store(){
   	$this->validate(request(),[
   		'name' => 'required',
   		'image' => 'required',
   		'link' => 'required'
   		]);
   	$clients = Partners::forceCreate([
		'name' =>request('name'),
		'image' =>request()->file('image')->store('clients','public'),
		'link' => request('link'),
   		]);
 	if(request()->expectsJson()){
 		return $partners;
 	}
  	return['message' => 'project Created'];
   }
   public function update(){

   	dd(request()->address);
   }
   public function destroy($id){
   	$client = Partners::find($id);
   	$client->delete();
   	if(request()->expectsJson()){

                return response(['partners' => 'Client deleted']);
      }
   }
}
