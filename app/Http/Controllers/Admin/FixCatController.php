<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\FixCat;

class FixCatController extends Controller
{
    public function __construct(){
             $this->middleware('auth:admin');
    }
    public function index(){
    	return FixCat::all();
    
    }
    public function create(Request $request){
    	$fixcat = new FixCat;
        $fixcat->name = $request->name;
        
        $fixcat->save();
    }
    public function store(Request $request){
        $fixcat = new FixCat;
        $fixcat->name = $request->name;
        
        $fixcat->save();
    }
    public function update(FixCat $fixcat){
        
        $fixcat->update([
            'name' => request('name'),
            ]);
    }
    public function destroy(FixCat $fixcat){
            if(request()->expectsJson()){
                return response(['status' => 'Category deleted']);
            }
    }

}
