<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Clients;

class ClientController extends Controller
{
	public function __construct(){

	}

   public function index(){
   	$clients = Clients::all();
   	return $clients;
   }
   public function store(){
   	$this->validate(request(),[
   		'name' => 'required',
   		'image' => 'required',
   		'telephone' => 'required|numeric',
   		'email' => 'required|email',
   		'address' => 'required'
   		]);
   	$clients = Clients::forceCreate([
		'name' =>request('name'),
		'image' =>request()->file('image')->store('clients','public'),
		'telephone' => request('telephone'),
		'email' => request('email'),
		'address' => request('address')
   		]);
 	if(request()->expectsJson()){
 		return $clients;
 	}
  	return['message' => 'project Created'];
   }
   public function update(){

   	dd(request()->address);
   }
   public function destroy($id){
   	$client = Clients::find($id);
   	$client->delete();
   	if(request()->expectsJson()){

                return response(['clients' => 'Client deleted']);
      }
   }
}
