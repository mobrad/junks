<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\FixCat;

class HomeController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }
    public function index(){
    	$fixcat = Fixcat::all();
        return view('admin.layouts.admin',compact('fixcat'));
    }
}
