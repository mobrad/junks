<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
 use Illuminate\Foundation\Auth\AuthenticatesUsers;
 use Auth;

class LoginController extends Controller
{
    // public function __construct(){
    //     $this->middleware('guest');
    // }
    public function showLoginForm(){
        return view('admin.login');
    }
    public function login(Request $request){
        //validate the login form
        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required|min:5'
        ]);
        
        //attempt to login
        if(Auth::guard('admin')->attempt(['email'=> $request->email,'password' => $request->password],$request->remember)){
            return redirect()->intended(route('admin.dashboard'));
        }

        //if successfull the redirect to their intended target

        //if unsuccessful then redirect to the login with the form data
        return redirect()->back()->withInput($request->only('email','remember'));
    }
    public function logout(){
        Auth::guard('admin')->logout();
        return redirect('/');
    }

}
