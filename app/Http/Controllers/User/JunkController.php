<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User\Junk;
use App\User\UserProfile;
use App\Admin\FixCat;
use Auth;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class JunkController extends Controller
{
  
  public function __construct() {
      $this->middleware('auth')->except('userLogout','junkCategory');
	}

   public function index() {
     	$junks = Junk::where('users_id','=', Auth::user()->id)->get();
     	return $junks;
   }
   //validates  and stores the junks
   public function store() {

       $this->validate(request(),[
         		'name' => 'required',
         		'image' => 'required',
            'price' => 'required',
            'quantity' => 'required|numeric',
            'category' => 'required|numeric',
         		]);

         	$junks = Junk::forceCreate([
        		'name' =>request('name'),
            'users_id' => auth()->user()->id,
            'price' => request('price'),
            'quantity' => request('quantity'),
            'cat_id'   => request('category'),
            'pricing' => request('pricing'),
            'description' =>request('description')
         		]);
         	
          if(request()->expectsJson()){
         		return $junks;
         	}
        	return['message' => 'project Created'];
   }

   public function update($id){
    if(request()->file('image') !== null){
   	 $junk = Junk::where('id' ,'=' ,$id)->update([
        'name' => request('name'),
        'category' => request('category'),
        'price' => request('price'),
        'quantity' => request('quantity'),
        'description' => request('description'),
        'image' => request()->file('image')->store('junks','public'),
      ]);
    }else {
      $junk = Junk::where('id' ,'=' ,$id)->update([
        'name' => request('name'),
        'cat_id' => request('category'),
        'price' => request('price'),
        'quantity' => request('quantity'),
        'description' => request('description'),
      ]);
      dd(request('description'));
    }
   }

   public function destroy($id){
   	$junks = Junk::find($id);
   	$junks->delete();
   	if(request()->expectsJson()){
                return response(['junks' => 'junks deleted']);
      }
   }
   //returns the categories
   public function category(){
     $category = FixCat::pluck('name','id');
     return $category;
   }
   
   //returns the sell view
   public function sell(){
      $cat = FixCat::pluck('name');
      return view('site.sell',compact('cat'));
   }

   //it picks the junks that are found only in the category
   public function junkCategory($fixcat){
       $cat = FixCat::all();
       $junkcat = FixCat::pluck('name');
       foreach ($junkcat as $key => $value){
            if($fixcat == str_slug($value)){
              $fixcat   = FixCat::where('name','=',$value)->first();
              $junks = Junk::where('cat_id', '=', $fixcat->id)->paginate(10);
              
              return view('site.category',compact('junks','fixcat','cat'));
             }
        }
    }
    //Its responsible for handling the selling of junks without a dasboard
    public function sellJunk(){
        $img = cl_image_tag(Input::file('image'),["width"=>250, "height"=>250, "crop"=>"mfit"]);
        $this->validate(request(),[
            'name' => 'required',
            'image' => 'required',
            'price' => 'required',
            'telephone' => 'required|numeric',
            'quantity' => 'required|numeric',
            'category' => 'required|numeric',
            'address' => 'required',
            'locality' => 'required',
            'state' => 'required',
            'country' => 'required',
            'zip' => 'required',
            ]);
        //Creates the Junk

        dd(is_writable(storage_path('app\public\junks')));
        Junk->forceCreate([
          'name' =>request('name'),
          'users_id' => auth()->user()->id,
          'image' => $img->store('junks','public'),
          'price' => request('price'),
          'description' => request('description'),
          'quantity' => request('quantity'),
          'cat_id'   => request('category'),
          'pricing' => request('pricing'),
          ]);

          $profile = UserProfile::where('user_id' ,'=' ,auth()->user()->id)->update([
            'address' => request('address'),
            'telephone' => request('telephone'
            ),
            'state' => request('state'),
            'locality' => request('locality'),
            'country' => request('country'),
            'zip' => request('zip'),
            ]);
          dd($profile);
    }
}
