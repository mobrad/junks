<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User\UserProfile;
use App\User;

class ProfileController extends Controller
{
	public function __construct(){

	}

    public function show($id){
    	$user = User::find($id);
    	return $user;

    }

    public function update($id){
    	auth()->user()->profile->update([
    		'name' => request()->input('name'),
    		'gender' => request()->input('gender'),
    		'telephone' => request()->input('telephone'),
    		'address' => request()->input('address'),
    		'country' => request()->input('country'),
    		'zip' => request()->input('zip'),
    		]);
    }
    public function image($id){
    	// dd(request()->file('avatar')->store());
    	// $image = request()->file('avatar')->store('user');
    	// $profile_image = User::find($id)->profile->image;
    	// $profile_image = $image;
    	// $profile_image->save();
    	auth()->user()->profile->update([
    		'image' => request()->file('avatar')->store('user','public')
    		]);

    	
    }
}
