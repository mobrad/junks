<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User\Junk;
use App\Admin\FixCat;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $junk = Junk::paginate(20);
        $cat = FixCat::all();
        return view('welcome',compact('junk','cat'));
    }

    public function show($fixcat_id,Junk $junk){
        if(Junk::findorFail($junk->id)){
            $this->view($junk->id);
        }
        $junks = Junk::paginate(10);
        $junk = Junk::find($junk->id);
        return view('site.junk',compact('junk','junks'));
    }
    public function view($id){
        $index = null;
        $view = new \App\User\View;
        $view->junks_id = $id;
        $view->visitor = request()->ip();
        $compare = $view->where('junks_id','=',$id)->pluck('junks_id');
        foreach ($compare as $compared){
             $index = $compared;
             }
             if($index == $id){
                $view->where('junks_id','=', $index)->increment('amount');
                return 'done';
             }else{ 
                $view->save(); 
                return 'no done'; 
             }//
    }
}
